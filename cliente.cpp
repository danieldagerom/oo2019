#include <iostream>
#include "cliente.hpp"
    //using namespace std;
Cliente::Cliente(){
    nome = "";
    idade = 0;
    cpf = 0;
    email = "email.com";
    std::cout << "Construtor da classe Cliente" << std::endl;
}
Cliente::~Cliente(){
    std::cout << "Destrutor da clase Cliente" << std::endl;
}
void Cliente::set_nome(string nome){
    this -> nome = nome;
}
string Cliente::get_nome(){
    return nome;
}
void Cliente::set_idade(int idade){
    this -> idade = idade;
}
int Cliente::get_idade(){
    return idade;
}
void Cliente::set_cpf(long int cpf){
    this -> cpf = cpf;
}
long int Cliente::get_cpf(){
    return cpf;
}
void Cliente::set_email(string email){
    this -> email = email;
}
string Cliente::get_email(){
    return email;
}
void Cliente::imprime_dados(){
    std::cout << "Nome : " << nome << std::endl;
    std::cout << "Idade: " << idade << std::endl;
    std::cout << "Cpf : " << cpf << std::endl;
    std::cout << "Email : " << email << std::endl;
}